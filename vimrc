" John French's Personal Vim Configuration

" Display Line Numbers
set number

" Set Tab Properties
set tabstop=2
set softtabstop=0 noexpandtab
set shiftwidth=2

" Change cursor based on Vim Mode
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" Plugins
call plug#begin('~/.vim/plugged')

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" One Dark Vim Theme
Plug 'rakr/vim-one'

" Base16 Vim Theme
Plug 'chriskempson/base16-vim'

" Autoclose Brackets
Plug 'Townk/vim-autoclose'

" Autoclose HTML - then press > again to indent
Plug 'alvan/vim-closetag'

" Vim Vinegar - switch/open files
Plug 'tpope/vim-vinegar'

" Vim Plolyglot Language Pack
Plug 'sheerun/vim-polyglot'

" Nunjucks Language
Plug 'lepture/vim-jinja'

" Lightline Info Bar
Plug 'itchyny/lightline.vim'
set laststatus=2
let g:lightline = {
	\ 'colorscheme': 'seoul256',
	\ }


call plug#end()

"Credit joshdick
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
  "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif


set background=dark " for the dark version
" set background=light " for the light version
colorscheme base16-atelier-plateau

" Hide the ~ below the line numbers
hi nontext ctermfg=bg guifg=bg cterm=NONE gui=NONE



